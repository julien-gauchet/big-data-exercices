package fr.julien.formations.pig.udf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputFormat;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.log4j.Logger;
import org.apache.pig.LoadFunc;
import org.apache.pig.PigException;
import org.apache.pig.backend.executionengine.ExecException;
import org.apache.pig.backend.hadoop.executionengine.mapReduceLayer.PigSplit;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;
import org.apache.pig.impl.PigContext;

public class ChargementPrenoms extends LoadFunc{
    
    private static final Logger LOG = Logger.getLogger(ChargementPrenoms.class);
    private RecordReader<?, ?> reader = null;

    @Override
    public InputFormat<?, ?> getInputFormat() throws IOException {
        InputFormat<?, ?> res = null;
        String inputFormatClassName = TextInputFormat.class.getName();
        try {
            res = (InputFormat<?, ?>) PigContext.resolveClassName(inputFormatClassName).newInstance();
        }
        catch (InstantiationException | IllegalAccessException e) {
            LOG.error(e.getMessage(), e);
        }
        return res;
    }

    @Override
    public Tuple getNext() throws IOException {
        Tuple res = null;
        try {
            if (!reader.nextKeyValue()) {
                reader.close();
                return null;
            }
            Text value = (Text) reader.getCurrentValue();
            String line = value.toString();
            List<String> champs = genererChamps(line);
            if (champs != null) {
                res = TupleFactory.getInstance().newTuple(champs);
            }
        }
        catch (InterruptedException e) {
            throw new ExecException(e.getMessage(), 202, PigException.REMOTE_ENVIRONMENT, e);
        }
        return res;
    }

    protected List<String> genererChamps(String line) {
        List<String> champs = new ArrayList<>();
        champs.add(line.substring(0, 31).trim().toLowerCase());
        champs.add(line.substring(31, 36).trim());
        champs.add(line.substring(36, line.length()).trim().toLowerCase());
        return champs;
    }

    @Override
    public void prepareToRead(@SuppressWarnings("rawtypes") RecordReader recordReader, PigSplit arg1) throws IOException {
        reader = recordReader;
    }

    @Override
    public void setLocation(String location, Job job) throws IOException {
        FileInputFormat.setInputPaths(job, location);
    }
}
