package fr.julien.formations.hive.udf;

import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.exec.UDFArgumentLengthException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.StringObjectInspector;

public class OrigineFrancaiseUDF extends GenericUDF {

    private StringObjectInspector origineInspector;

    @Override
    public Object evaluate(DeferredObject[] arguments) throws HiveException {
        String res = "0";
        if (arguments[0] != null) {
            String value = origineInspector.getPrimitiveJavaObject(arguments[0].get());
            if (value != null && value.matches(".*french.*")) {
                res = "1";
            }
        }
        return res;
    }

    @Override
    public String getDisplayString(String[] arg0) {
        return "UDF de classement des origines en francaise : 1 ou étrangères : 0";
    }

    @Override
    public ObjectInspector initialize(ObjectInspector[] arguments) throws UDFArgumentException {
        if (arguments.length != 1) {
            throw new UDFArgumentLengthException("La fonction doit contenir un paramètre seulement");
        }
        ObjectInspector a = arguments[0];
        if (!(a instanceof StringObjectInspector)) {
            throw new UDFArgumentException("Le premier argument doit être un String");
        }
        origineInspector = (StringObjectInspector) a;
        return PrimitiveObjectInspectorFactory.javaStringObjectInspector;
    }
}
